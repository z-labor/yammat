{
  description = "Matemat";

  outputs = { self, nixpkgs }:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];
      forSystems = nixpkgs.lib.genAttrs systems;
    in rec {
      packages = forSystems (system: {
        yammat = nixpkgs.legacyPackages.${system}.haskellPackages.callPackage ./pkg.nix {};
      });

      defaultPackage = forSystems (system:
        self.packages.${system}.yammat
      );

      checks = packages;

      hydraJobs =
        let
          system = builtins.head systems;
          pkgs = nixpkgs.legacyPackages.${system};
          ghcVersions =
            builtins.filter (p:
              builtins.match "ghc.+" p != null
            ) (builtins.attrNames pkgs.haskell.packages);
        in
          builtins.foldl' (checks: ghcVersion:
            let
              haskellPackages = pkgs.haskell.packages.${ghcVersion};
              inherit (pkgs.haskell.lib.compose) justStaticExecutables;
            in
            checks // {
              "${ghcVersion}".yammat = justStaticExecutables(haskellPackages.callPackage ./pkg.nix {});
            }
          ) {} ghcVersions;

      devShell = forSystems (system:
        import ./shell.nix {
          nixpkgs = nixpkgs.legacyPackages.${system};
        }
      );

      nixosModule = import ./nixos-module.nix;
    };
}
