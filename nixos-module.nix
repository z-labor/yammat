{ config, lib, pkgs, ... }:

let
  yammat = import ./. { inherit pkgs; };
  cfg = config.services.yammat;
in
{
  options.services.yammat = with lib; {
    enable = mkEnableOption "Yammat";
    user = mkOption {
      type = types.str;
      default = "yammat";
      description = "System user to run Yammat";
    };

    group = mkOption {
      type = types.str;
      default = "yammat";
      description = "System group to run Yammat";
    };

    config = mkOption {
      type = types.lines;
      default = builtins.readFile ./config/settings.yml + ''
        sendmail-location: "/run/wrappers/bin/sendmail"
      '';
      description = "Configuration for Yammat";
    };
  };

  config = lib.mkIf cfg.enable {
    users.users.${cfg.user} = {
      isSystemUser = true;
      group = cfg.group;
    };
    users.groups.${cfg.group} = {};

    services.postgresql = {
      enable = true;
      ensureDatabases = [ "yammat" ];
      ensureUsers = [ {
        name = cfg.user;
        ensurePermissions = {
          "DATABASE yammat" = "ALL PRIVILEGES";
        };
      } ];
    };

    systemd.services.yammat = {
      description = "Yammat Matemat";
      after = [ "postgresql.service" ];
      requires = [ "postgresql.service" ];
      wantedBy = [ "multi-user.target" ];
      environment = {
        APPROOT = yammat;
        STATIC_DIR = "${yammat}/static";
        PGHOST = "";
      };
      # make sendmail available
      path = [ "/run/wrappers" ];
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        WorkingDirectory = yammat;
        ExecStart = "${yammat}/bin/yammat /etc/yammat.yml";
      };
    };

    environment.etc."yammat.yml" = {
      enable = true;
      text = cfg.config;
    };
  };
}
